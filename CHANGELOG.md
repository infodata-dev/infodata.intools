# Infodata - IN Tools Extension change Log.

0.1.0 : creation
0.1.3 : update to use vscode marketplace 
0.1.10 : fix to escape $ which is not snippet-variables and snippet-placeholders in snippets/body : (error : one or more snippets from the extension 'intools' very likely confuse snippet-variables and snippet-placeholders).
