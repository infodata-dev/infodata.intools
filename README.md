# README #

## What is this repository for? ###
This extension aims to implement all the specificities of use of VSCode by Infodata teams.
It offers 'contributes' on rocket-mvbasic extensions, infodata.sbextension, liximomo.sftp, etc.

c.f. https://wiki.infodata.lu/devtools/vscode/vsc4sbparagraph 
c.f. https://bitbucket.org/infodata-dev/infodata.intools/src/master/

### Content

## Snippets
- INBASIC, INBASIC.UV, INPROGS (non pd.p) : Cross Infodata's app subroutines.
- WIBASIC : infodata's WEB Interafce libraries 
- uniBASIC : (rocket-mvbasic) to help input statements and calls 
- INTools : standard for coding 

### Who do I talk to? ###

* Repo owner or admin : efv@infodata.lu
* Other community or team contact : jcd@infodata.lu 

